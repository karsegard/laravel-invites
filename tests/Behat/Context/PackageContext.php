<?php

namespace KDA\Tests\Behat\Context;


/**
 * Defines application features from the specific context.
 */
class PackageContext extends BaseContext
{
    use Concerns\Factory;
    use Concerns\Models;
    use Concerns\Notifications;
    use Concerns\Invitation;
    use Concerns\Route;
}
