<?php

namespace KDA\Tests\Behat\Context\Concerns;
use Illuminate\Support\Facades\Notification;
use Behat\Gherkin\Node\PyStringNode;
use KDA\Laravel\Invites\Models\Invitation as ModelsInvitation;
use KDA\Laravel\Invites\Notifications\VerifyInvitationEmail;

/**
 * Defines application features from the specific context.
 */
trait Invitation 
{

    protected $invitation;
     /**
     * @Given an invitation with payload
     */
    public function anInvitationWithPayload(PyStringNode $string)
    {
        $this->theModelClassIs(ModelsInvitation::class);
        $this->theFollowingFactoryAttributes($string);
        $this->craftingARecord();
        $this->invitation = $this->factory_record;
    }

     /**
     * @When I send the invitation notification
     */
    public function iSendTheInvitationNotification()
    {
        $this->invitation->sendInvitation();
    }
    /**
     * @Then assert Notification is sent to invitation email
     */
    public function assertNotificationIsSentToInvitationEmail()
    {
        Notification::assertSentTo($this->invitation,VerifyInvitationEmail::class,function($notification){
            $mailData = $notification->toMail($this->invitation);
            dump($mailData);
            return true;
        });
    }



}
