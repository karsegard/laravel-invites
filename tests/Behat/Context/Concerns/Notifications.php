<?php

namespace KDA\Tests\Behat\Context\Concerns;
use Illuminate\Support\Facades\Notification;
use Behat\Gherkin\Node\PyStringNode;

/**
 * Defines application features from the specific context.
 */
trait Notifications 
{
/**
     * @Given using fake notifications
     */
    public function usingFakeNotifications()
    {
        Notification::fake();
    }
}
