<?php

namespace KDA\Tests\Behat\Context\Concerns;
use Illuminate\Support\Facades\Notification;
use Behat\Gherkin\Node\PyStringNode;
use Illuminate\Support\Facades\Route as FacadesRoute;

/**
 * Defines application features from the specific context.
 */
trait Route 
{
  /**
     * @Given having a :method route on :url named :name
     */
    public function havingARouteOnNamed($method, $url, $name)
    {
        FacadesRoute::$method($url,function(){
            return 'testing';
        })->name($name);
    }
}
