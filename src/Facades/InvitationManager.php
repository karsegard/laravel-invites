<?php

namespace KDA\Laravel\Invites\Facades;

use Illuminate\Support\Facades\Facade;

class InvitationManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
