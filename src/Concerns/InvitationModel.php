<?php
namespace KDA\Laravel\Invites\Concerns;

use Closure;
use KDA\Laravel\Invites\Models\Invitation;

trait InvitationModel{

  
    protected Invitation $invitation;

    public function invitation($invitation):static
    {
        $this->invitation = $invitation;
        return $this;
    }

    public function createInvite($attributes):static
    {
        $this->invitation(Invitation::create($attributes));
        return $this;
    }

    public function getInvitation():Invitation
    {
        return $this->invitation;
    }

    
}