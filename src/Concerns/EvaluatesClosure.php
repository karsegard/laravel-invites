<?php

namespace KDA\Laravel\Invites\Concerns;

use Closure;
use Illuminate\Support\Collection;
trait EvaluatesClosure
{
    public function evaluate($value, array | Collection $parameters = [])
    {
        if($parameters instanceof Collection){
            $parameters = $parameters->all();
        }
        if ($value instanceof Closure) {
            return app()->call(
                $value,
                $parameters
            );
        }

        return $value;
    }

    public function getEvaluationParameters(array $exceptKeys=[]):Collection
    {
        $args =  collect([
           
        ]);
        return $args;
    }
    
}
