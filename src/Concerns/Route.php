<?php
namespace KDA\Laravel\Invites\Concerns;

use Closure;

trait Route{

    protected Closure | string $invitation_route;
    public function getInvitationRouteUsing(Closure | string $route):static
    {
        $this->invitation_route = $route;
        return $this;
    }

    public function getInvitationRoute(){
        return $this->evaluate($this->invitation_route,[]);
    }
    
}