<?php
namespace KDA\Laravel\Invites\Concerns;

use Closure;

trait GeneratesUrl{


    protected Closure $generateUrlUsing ;

    public function generateUrl(): string 
    {
        return $this->evaluate($this->generateUrlUsing,["invite"=>$this->getInvitation()]);
    }
    
    protected function generateUrlUsing(Closure $callback):static
    {
        $this->generateUrlUsing=$callback;
        return $this;
    }
}