<?php
namespace KDA\Laravel\Invites;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\URL;
use KDA\Invites\Notifications\VerifyInvitationEmail;
use KDA\Laravel\Invites\Concerns\EvaluatesClosure;
use KDA\Laravel\Invites\Concerns\GeneratesToken;
use KDA\Laravel\Invites\Concerns\GeneratesUrl;
use KDA\Laravel\Invites\Concerns\InvitationModel;
use KDA\Laravel\Invites\Concerns\Route;
use KDA\Laravel\Invites\Models\Invitation;
use KDA\Laravel\Traits\HasTranslations;

//use Illuminate\Support\Facades\Blade;
class InvitationManager 
{
    use GeneratesUrl;
    use Route;
    use InvitationModel;
    use HasTranslations;
    use EvaluatesClosure;
    public function __construct()
    {
        $this->generateUrlUsing(function($invite){
            return URL::temporarySignedRoute(
                $this->getInvitationRoute(),
                Carbon::now()->addMinutes(config('kda.laravel-invites.expire', 60)),
                [
                    'id' => $invite->getKey(),
                    'hash' => sha1($invite->getVerificationString()),
                ]
            );
        });
        $this->getInvitationRouteUsing(config('kda.laravel-invites.route', 'invitation.verify'));
    }


    public function notify(){
        $this->getInvitation()->sendInvitation();
    }

}