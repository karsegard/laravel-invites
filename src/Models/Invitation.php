<?php

namespace KDA\Laravel\Invites\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use KDA\Laravel\Invites\Notifications\VerifyInvitationEmail;
use KDA\Laravel\Invites\ServiceProvider;

class Invitation extends Model
{
    use HasFactory;
    use Notifiable;

    protected $fillable = [
        'email','expired_at','token','payload'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       'payload'=>'json'
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Laravel\Invites\Database\Factories\InvitationFactory::new();
    }

    public function getTable()
    {
        return ServiceProvider::getTableName('invitations')        ;
    }

    public function getVerificationString():string
    {
        return $this->email;
    }
    public function sendInvitation(){
        $this->notify(new VerifyInvitationEmail);
    }
}
