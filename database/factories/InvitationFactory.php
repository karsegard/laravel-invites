<?php

namespace KDA\Laravel\Invites\Database\Factories;

use KDA\Laravel\Invites\Models\Invitation;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvitationFactory extends Factory
{
    protected $model = Invitation::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
