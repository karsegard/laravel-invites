<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\Invites\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create(ServiceProvider::getTableName('invitations'), function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->boolean('sent')->default(false);
            $table->nullableNumericMorphs('origin');
            $table->nullableNumericMorphs('registerd');
            $table->timestamp('expired_at')->nullable();
            $table->json('payload')->nullable();
            $table->timestamps();

        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(ServiceProvider::getTableName('invitations'));
     
        Schema::enableForeignKeyConstraints();
    }
};