

- An invitation needs an origin, by default it's the authenticated user.

- An invitation link expires. Origin user needs to manually resend the email if expired.

- An invitation link view should be as close as possible as the register view.

