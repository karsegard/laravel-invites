Feature: Factories
    Background: Customer
    Given the model class is "KDA\Laravel\Invites\Models\Invitation"

    Scenario: Model can be created
    Given the following factory attributes 
    """
    {
        "email":"fabien@karsegard.ch"
    }
    """
    And crafting a record 
    Then The record is not null
