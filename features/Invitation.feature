Feature: Invitation
    Background: Invitation
        Given using fake notifications
        And having a "get" route on "/invitation" named "invitation.verify"

    Scenario: I can send invitation to user
        And an invitation with payload 
         """
        {
            "email":"fabien@karsegard.ch"
        }
        """
        When I send the invitation notification
        Then assert Notification is sent to invitation email