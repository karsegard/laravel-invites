<?php
// config for KDA/Laravel\Invites
return [
    'expire'=>60,
    'route'=>'invitation.verify',
    'tables'=>[
        'invitations'=>'kda_invitations'
    ]
];
